# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=libabigail
pkgver=1.6
pkgrel=0
pkgdesc="The ABI Generic Analysis and Instrumentation Library"
url="https://sourceware.org/libabigail/"
arch="all"
options="!check"  # Relies on checking non-reproducible binaries.
license="LGPL-3.0+"
depends=""
depends_dev=""
makedepends="$depends_dev elfutils-dev fts-dev libxml2-dev"
subpackages="$pkgname-dev"
source="http://mirrors.kernel.org/sourceware/libabigail/$pkgname-$pkgver.tar.gz
	musl-basename.patch
	musl-fts.patch
	redhat-bs.patch"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2ba775bd836b53265dae60e66065fc2106e7f633e890cc70b6abacb8426e04ec0b7b38593c21336f5785cf5e4eb8d110fb15ce63124cddc59f660f31c7b25b3c  libabigail-1.6.tar.gz
ef5c7db0ce05dff36c654e4a6345ded43032b76bdc9159da60a5fdc59b3c70943881cb19c87a4c6b30eccb774c6c7767e790b395564058f5dbaa6e3f3e0b1373  musl-fts.patch
0f5dcee3fbb1ee949df22e13a8ad248a8e39a086cb4c3561f5f45235c2f27cc589a07886560e2ecf7ee040811f64892c0e74ed653e0a1a5d20e62ab360c388a4  redhat-bs.patch"
