# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdelibs4support
pkgver=5.68.0
pkgrel=0
pkgdesc="Legacy support for KDE 4 software"
url="https://www.kde.org/"
arch="all"
options="!check suid"  # Test requires running X11 session.
license="LGPL-2.1+ AND MIT AND LGPL-2.1-only AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only) AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev kcompletion-dev
	kconfigwidgets-dev kcrash-dev kdesignerplugin kdesignerplugin-dev
	kemoticons-dev kparts-dev kunitconversion-dev kinit kded kded-dev
	kitemmodels-dev knotifications-dev sonnet-dev threadweaver-dev
	kinit-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdelibs4support-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9862e320f028e4a80f33db4532b9e865a8b8d642d434e064988fc1f72d9270e3f5883c17caca61f307b86dbf24b4a070bf27ddf12f1777d84a742749d0b8dc42  kdelibs4support-5.68.0.tar.xz"
