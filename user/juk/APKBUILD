# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=juk
pkgver=19.12.3
pkgrel=0
pkgdesc="KDE Jukebox"
url="https://juk.kde.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kcompletion-dev kcrash-dev kglobalaccel-dev ki18n-dev
	kiconthemes-dev kdoctools-dev kio-dev kjobwidgets-dev ktextwidgets-dev
	knotifications-dev kxmlgui-dev kwallet-dev kwidgetsaddons-dev
	kwindowsystem-dev taglib-dev phonon-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/juk-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="39fcfeba4e2616390ac55414bfe9030285765335973fc881b6fc83657ed868e354e0935b00acf669e6bc11e8f6f1daec6c67873d1de71eb14e7f75d1c05ffedb  juk-19.12.3.tar.xz"
