# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=baloo
pkgver=5.68.0
pkgrel=0
pkgdesc="Semantic desktop search framework"
url="https://www.kde.org/"
arch="all"
# Test #4 in the suite, bin/querytest:
# Test cleanup method is not run after testTermEqual.
# This leaves a `db` handle stale, which is using the single-reader-per-thread
# pthread_key that lmdb provides.
# This causes the next test, testTermStartsWith, to fail to acquire a reader
# transaction because the previous transaction was not committed or aborted.
# Since the test does not check for a transaction failure and blindly uses the
# transaction object returned, it attempts to access 0xc (accessing a member of
# the returned nullptr), and segfaults.
# Three other tests in the suite fail in the same way.
# This may be a bug in libQtTest, or in musl.  I have not debugged further.
# This has already taken up two days of my time and I just want to ship Plasma
options="!check"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1+"
depends=""
depends_dev="kcoreaddons-dev kfilemetadata-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qtbase-dev qt5-qttools-dev kconfig-dev kcrash-dev kdbusaddons-dev
	ki18n-dev kidletime-dev kio-dev lmdb-dev solid-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/baloo-$pkgver.tar.xz
	initialise-variable.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="54aa3d9a8e135fbc76b0583b2e5261bf027163addafabc7179e990a6c8c92eadc49195b6f66d77408a3ad74c2f7a3ab4328bc7afb243d1be519e78d0223ea2a6  baloo-5.68.0.tar.xz
a65c36808c56dff4bc3bb91e791fcc9cba3cdaca686a214adf51e4d87d86ef7ee07d9dd8e0634dfc53a732b77668c6942322c993be5054bc4fc51e7bd69908ac  initialise-variable.patch"
