# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-mime
pkgver=19.12.3
pkgrel=0
pkgdesc="Libraries to implement basic MIME message handling"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev akonadi-dev kio-dev kmime-dev"
makedepends="$depends_dev cmake extra-cmake-modules libxslt-dev kdbusaddons-dev
	kconfig-dev kitemmodels-dev kxmlgui-dev shared-mime-info"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-mime-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# The mailserializerplugintest test requires kdepim-runtime to be
	# present on the system.  Otherwise, Akonadi does not know how to
	# convert a QByteArray of message/rfc822 to KMime::Message.  This
	# is a circular dependency, because kdepim-runtime requires amime
	# so we must skip this test.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'mailserializerplugintest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d8c22ae1a2a349990248f209be017ceb2976bbb138ed25dc1e67993738d91c426897371f0844e6baecf095b5b39240b8dd836babb970c50b61af3aecc45dc251  akonadi-mime-19.12.3.tar.xz"
