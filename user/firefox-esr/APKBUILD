# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=firefox-esr
pkgver=68.9.0
pkgrel=0
pkgdesc="Firefox web browser (extended support release)"
url="https://www.mozilla.org/firefox/"
arch="all"
options="!check"  # Tests disabled
license="MPL-2.0"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 cargo cbindgen clang llvm8-dev node ncurses-dev 
	perl rust rust-std cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev fts-dev gconf-dev gtk+3.0-dev hunspell-dev
	libnotify-dev libsm-dev libxcomposite-dev libxdamage-dev
	libxrender-dev libxt-dev nasm nss-static sqlite-dev
	startup-notification-dev unzip yasm zip gtk+2.0-dev
	"
_py2ver="2.7.16"
_ffxver="$pkgver"'esr'
source="https://ftp.mozilla.org/pub/firefox/releases/$_ffxver/source/firefox-$_ffxver.source.tar.xz
	https://www.python.org/ftp/python/$_py2ver/Python-$_py2ver.tar.xz
	mozconfig

	bad-google-code.patch
	disable-gecko-profiler.patch
	fix-mutex-build.patch
	fix-seccomp-bpf.patch
	jpeg-link.patch
	mozilla-build-arm.patch
	ppc32-fix.patch
	rust-32bit.patch
	seccomp-musl.patch
	seccomp-time64.patch
	shut-up-warning.patch
	skia-sucks1.patch
	skia-sucks2.patch
	skia-sucks3.patch
	stackwalk-x86-ppc.patch
	webrtc-broken.patch

	firefox.desktop
	firefox-safe.desktop
	"
builddir="$srcdir/firefox-$pkgver"
_mozappdir=/usr/lib/firefox
ldpath="$_mozappdir"

# secfixes: firefox-esr
#   68.0.2-r0:
#     - CVE-2019-11733
#   68.1.0-r0:
#     - CVE-2019-9812
#     - CVE-2019-11735
#     - CVE-2019-11736
#     - CVE-2019-11738
#     - CVE-2019-11740
#     - CVE-2019-11742
#     - CVE-2019-11743
#     - CVE-2019-11744
#     - CVE-2019-11746
#     - CVE-2019-11747
#     - CVE-2019-11748
#     - CVE-2019-11749
#     - CVE-2019-11750
#     - CVE-2019-11751
#     - CVE-2019-11752
#     - CVE-2019-11753
#   68.2.0-r0:
#     - CVE-2019-15903
#     - CVE-2019-11757
#     - CVE-2019-11758
#     - CVE-2019-11759
#     - CVE-2019-11760
#     - CVE-2019-11761
#     - CVE-2019-11762
#     - CVE-2019-11763
#     - CVE-2019-11764
#   68.3.0-r0:
#     - CVE-2019-11745
#     - CVE-2019-13722
#     - CVE-2019-17005
#     - CVE-2019-17008
#     - CVE-2019-17009
#     - CVE-2019-17010
#     - CVE-2019-17011
#     - CVE-2019-17012
#   68.4.1-r0:
#     - CVE-2019-17016
#     - CVE-2019-17017
#     - CVE-2019-17022
#     - CVE-2019-17024
#     - CVE-2019-17026
#   68.5.0-r0:
#     - CVE-2020-6796
#     - CVE-2020-6797
#     - CVE-2020-6798
#     - CVE-2020-6799
#   68.6.0-r0:
#     - CVE-2019-20503
#     - CVE-2020-6805
#     - CVE-2020-6806
#     - CVE-2020-6807
#     - CVE-2020-6811
#     - CVE-2020-6812
#     - CVE-2020-6814
#   68.7.0-r0:
#     - CVE-2020-6819
#     - CVE-2020-6820
#     - CVE-2020-6821
#     - CVE-2020-6822
#     - CVE-2020-6825
#   68.8.0-r0:
#     - CVE-2020-6831
#     - CVE-2020-12387
#     - CVE-2020-12392
#     - CVE-2020-12395
#   68.9.0-r0:
#     - CVE-2020-12399
#     - CVE-2020-12405
#     - CVE-2020-12406
#     - CVE-2020-12410

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-$_py2ver"
	[ -d ../python ] && rm -r ../python

	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence

	sed -e 's/é/e/g' /etc/os-release > "$srcdir"/os-release
	export UNIXCONFDIR="$srcdir"

	./configure --prefix="$srcdir/python" --with-ensurepip=install
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install

	# firefox's bundled pipenv and pip aren't new enough to support
	# configurable UNIXCONFDIR
	export PATH="$srcdir/python/bin:$PATH"
	pip2 install virtualenv pipenv
}

prepare() {
	default_prepare
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	echo "mk_add_options MOZ_MAKE_FLAGS=\"-j$JOBS\"" >> "$builddir"/mozconfig

	case "$CARCH" in
		pmmx|x86*)
			echo "ac_add_options --disable-elf-hack" >> "$builddir"/mozconfig
			;;
		ppc)
			echo "ac_add_options --disable-webrtc" >> "$builddir"/mozconfig;
			export LDFLAGS="$LDFLAGS -latomic"
			;;
		s390x)
			echo "ac_add_options --disable-startupcache" >> "$builddir"/mozconfig
			;;
	esac

	rm "$builddir"/third_party/python/virtualenv/virtualenv_support/pip*.whl
	rm "$builddir"/third_party/python/virtualenv/virtualenv_support/setuptools*.whl
	cp "$srcdir/Python-$_py2ver"/Lib/ensurepip/_bundled/*.whl \
		"$builddir/third_party/python/virtualenv/virtualenv_support"
}

build() {
	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	# gcc 6
	export CXXFLAGS="-fno-delete-null-pointer-checks -fno-schedule-insns2"

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	export UNIXCONFDIR="$srcdir"

	export PATH="$srcdir/python/bin:$PATH"
	./mach build
}

run() {
	cd "$builddir"/obj-$CHOST/dist/bin
	export LD_LIBRARY_PATH=.
	./firefox -no-remote -profile "$builddir"/obj-$CHOST/tmp/profile-default
}

package() {
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	for png in browser/branding/official/default*.png; do
		local i="${_png%.png}"
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/firefox.png
	done

	install -m644 "$builddir"/browser/branding/official/default48.png \
		${pkgdir}/usr/share/pixmaps/firefox.png
	install -m644 ${startdir}/firefox.desktop \
		${pkgdir}/usr/share/applications/firefox.desktop
	install -m644 ${startdir}/firefox-safe.desktop \
		${pkgdir}/usr/share/applications/firefox-safe.desktop

	# install our vendor prefs
	install -d "$pkgdir"/$_mozappdir/browser/defaults/preferences

	cat >> "$pkgdir"/$_mozappdir/browser/defaults/preferences/firefox-branding.js <<- EOF
	// Use LANG environment variable to choose locale
	pref("intl.locale.matchOS", true);

	// Disable default browser checking.
	pref("browser.shell.checkDefaultBrowser", false);

	// Don't disable our bundled extensions in the application directory
	pref("extensions.autoDisableScopes", 11);
	pref("extensions.shownSelectionUI", true);
	EOF
}

sha512sums="98431800d80f7c680aef9eede29df8217810912a319a7f7f8c2e637c43ecd4f4e29223a417afb2a6315e825f979453ff6e6b5a575649aba5cc63ce5956375bb8  firefox-68.9.0esr.source.tar.xz
16e814e8dcffc707b595ca2919bd2fa3db0d15794c63d977364652c4a5b92e90e72b8c9e1cc83b5020398bd90a1b397dbdd7cb931c49f1aa4af6ef95414b43e0  Python-2.7.16.tar.xz
f82758d279cd12a1b30a9b36ac3c265cfb137df3db7ae185f2c538504e46fa70ace1b051fce847356851062b5cc9cd741a6d33d54f8cd103aa0c8272cb19ccc4  mozconfig
ace7492f4fb0523c7340fdc09c831906f74fddad93822aff367135538dacd3f56288b907f5a04f53f94c76e722ba0bab73e28d83ec12d3e672554712e6b08613  bad-google-code.patch
9c14041f0295682b8dbeb6d5b58a2f9dc0a2dc8bef995a0f7e30fa0b17c51aa0f6748f80fb8584169db7687e2eeb404dff68a09158ae56a5f24eef30685dd2b3  disable-gecko-profiler.patch
c0b2bf43206c2a5154e560ef30189a1062ae856861b39f52ce69002390ff9972d43e387bfd2bf8d2ab3cac621987bc042c8c0a8b4cf90ae05717ca7705271880  fix-mutex-build.patch
70863b985427b9653ce5e28d6064f078fb6d4ccf43dd1b68e72f97f44868fc0ce063161c39a4e77a0a1a207b7365d5dc7a7ca5e68c726825eba814f2b93e2f5d  fix-seccomp-bpf.patch
de8e3b15cd7dffb0eca5a729434986e5916234914cdc5fdcdbbc67d8bb439a535ed932293518dd74c3be07555ed60c9541d6758cd2f69d27c22965f7a076e4e3  jpeg-link.patch
e61664bc93eadce5016a06a4d0684b34a05074f1815e88ef2613380d7b369c6fd305fb34f83b5eb18b9e3138273ea8ddcfdcb1084fdcaa922a1e5b30146a3b18  mozilla-build-arm.patch
06a3f4ee6d3726adf3460952fcbaaf24bb15ef8d15b3357fdd1766c7a62b00bd53a1e943b5df7f4e1a69f4fae0d44b64fae1e027d7812499c77894975969ea10  ppc32-fix.patch
7c615703dc9b8427eeadd13bc9beda02e1c3d986cac1167feaf48fdfdcc15b7456460d4d58f301054cf459242ee75bbcd76bf67e26c2a443bc5655975d24ca1b  rust-32bit.patch
efc77a320850e10e8b99e6fb5d3dd28a3044e287fd87efbdf95807de26a6885235b2d994743cb374345d91a0353abd70a5790b829e37b717b77605e24d4f7f98  seccomp-musl.patch
4b20dfa3ef3d470af069a274c53ea35c67d2d123f1b543ee243e7038ed94f5a1a6121f1f67713a9442e246b979c042f11efc7a6c32d0b8d3fd2c448dd1258733  seccomp-time64.patch
39ddb15d1453a8412275c36fc8db3befc69dffd4a362e932d280fb7fd1190db595a2af9b468ee49e0714f5e9df6e48eb5794122a64fa9f30d689de8693acbb15  shut-up-warning.patch
e751ffab263f03d4c74feebc617e3af115b1b53cf54fe16c3acc585eec67773f37aa8de4c19599fa6478179b01439025112ef2b759aa9923c9900e7081cb65a9  skia-sucks1.patch
9152bd3e6dc446337e6a2ed602279c620aedecc796ba28e777854c4f41fcf3067f9ebd086a4b63a6b76c2e69ec599ac6435b8eeda4f7488b1c45f69113facba4  skia-sucks2.patch
7a1fa27e060b2f025eaebbd39fb5c62960b62450241437e6d057d58cef9faf1cd1a85efe3b6a37b865d686ff18e90605ebea3089b26243f2d14876c2107106a6  skia-sucks3.patch
452b47b825294779f98ed46bc1065dad76b79ff453521ef049934a120f349c84a1c863b16af1828fe053059823da9690ec917c055ae02dcc5c80c54cad732448  stackwalk-x86-ppc.patch
be68f1387aa6677875a67106e2d6a9db470c934c943056d3b53391a63034235108e41945c53957db427d9cdc59f0aa2f9e6f2f8cd862e090e512a3ab9cbcc9a8  webrtc-broken.patch
f3b7c3e804ce04731012a46cb9e9a6b0769e3772aef9c0a4a8c7520b030fdf6cd703d5e9ff49275f14b7d738fe82a0a4fde3bc3219dff7225d5db0e274987454  firefox.desktop
5dcb6288d0444a8a471d669bbaf61cdb1433663eff38b72ee5e980843f5fc07d0d60c91627a2c1159215d0ad77ae3f115dcc5fdfe87e64ca704b641aceaa44ed  firefox-safe.desktop"
