# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivities
pkgver=5.68.0
pkgrel=0
pkgdesc="Runtime and library to organize work into separate activities"
url="https://api.kde.org/frameworks/kactivities/html/index.html"
arch="all"
license="GPL-2.0+ AND LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="kactivitymanagerd"
makedepends="cmake extra-cmake-modules doxygen graphviz boost-dev
	qt5-qtbase-dev qt5-qttools-dev qt5-qtdeclarative-dev kconfig-dev
	kcoreaddons-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="1490834305965cff0c8ddc4e38be4960a88266f10806478e41cd976f0461a83ae4b6a4e9f3566fd90d5afcf3f38a291609179bea0781b939831ce8f788acc552  kactivities-5.68.0.tar.xz"
