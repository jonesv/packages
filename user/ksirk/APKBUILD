# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksirk
pkgver=19.12.3
pkgrel=0
pkgdesc="Strategy game from KDE"
url="https://www.kde.org/applications/games/ksirk/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kdoctools-dev ki18n-dev kiconthemes-dev kio-dev
	knewstuff-dev kwallet-dev kwidgetsaddons-dev kxmlgui-dev
	libkdegames-dev phonon-dev qca-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksirk-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="105442d9c3229cda57d4bafa5f77b5fbf639d6488eabfe405ee82ce0365781efc17526a3dc903a1c730617474c61f037fd81059b72227575920769fba7bff8bb  ksirk-19.12.3.tar.xz"
