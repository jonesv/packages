# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=lxqt-panel
pkgver=0.14.1
pkgrel=0
pkgdesc="Panel for LXQt desktop"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0 kguiaddons-dev
	libdbusmenu-qt-dev kwindowsystem-dev solid-dev menu-cache-dev
	lxmenu-data liblxqt-dev>=${pkgver%.*}.0 alsa-lib-dev pulseaudio-dev
	lxqt-globalkeys-dev>=${pkgver%.*}.0 lm_sensors-dev libstatgrab-dev
	libsysstat-dev qt5-qttools-dev libxkbcommon-dev libxcomposite-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-panel/releases/download/$pkgver/lxqt-panel-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="e7cb361a13ac480cda50bc0549d5501f0d4f5bb6c8e257febf81550aaa88c0c55b615762aedb1f03fd654f79efe5aa2590b1dd592f6ae8c7e5a8d08086e0a687  lxqt-panel-0.14.1.tar.xz"
