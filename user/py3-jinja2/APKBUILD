# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Brandon Bergren <git@bdragon.rtk0.net>
pkgname=py3-jinja2
_pkgname=Jinja2
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=2.10.3
pkgrel=0
pkgdesc="A small but fast and easy to use stand-alone template engine written in pure python."
url="https://pypi.python.org/pypi/Jinja2"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-markupsafe"
makedepends="python3-dev"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# secfixes: jinja2
#   2.10.1-r0:
#     - CVE-2019-10906

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$builddir:$PYTHONPATH" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="c51c335450f46a467b0d40de1a51c149bdb4eb961ea888b64ff141e11b592b32e05040bfd9aa4a39892dda8d9d8cbf5a35b386ea16a247484d31b5b52eda1b8f  py3-jinja2-2.10.3.tar.gz"
