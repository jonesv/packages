# Maintainer: 
pkgname=lame
pkgver=3.100
pkgrel=0
pkgdesc="MP3 encoder and graphical frame analyzer"
url="http://lame.sourceforge.net/"
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="ncurses-dev"
([ $CBUILD_ARCH != "pmmx" ] && [ $CBUILD_ARCH != "x86_64" ]) || makedepends="$makedepends nasm"
subpackages="$pkgname-dev $pkgname-doc"
source="https://downloads.sourceforge.net/project/lame/lame/$pkgver/$pkgname-$pkgver.tar.gz"

# secfixes:ss
#   3.100-r0:
#     - CVE-2017-9410
#     - CVE-2017-9411
#     - CVE-2017-9412
#     - CVE-2015-9099
#   3.99.5-r6:
#     - CVE-2015-9099
#     - CVE-2015-9100
#     - CVE-2017-9410
#     - CVE-2017-9411
#     - CVE-2017-9412
#     - CVE-2017-11720

prepare() {
	default_prepare

	# fix for parallel builds
	mkdir -p libmp3lame/i386/.libs

	# fix for pic build with new nasm
	sed -i -e '/define sp/s/+/ + /g' libmp3lame/i386/nasm.h
}

build() {
	LIBS="-ltinfo" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-nasm \
		--disable-mp3x \
		--enable-shared \
		--with-pic

	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0844b9eadb4aacf8000444621451277de365041cc1d97b7f7a589da0b7a23899310afd4e4d81114b9912aa97832621d20588034715573d417b2923948c08634b  lame-3.100.tar.gz"
