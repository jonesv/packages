# Contributor: Gentoo Rust Maintainers <rust@gentoo.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=xsv
pkgver=0.13.0
pkgrel=0
pkgdesc="A fast CSV command line toolkit written in Rust"
url="https://github.com/BurntSushi/xsv"
arch="all"
license="MIT OR Unlicense"
depends=""
makedepends="cargo"
source=""

# dependencies taken from Cargo.lock
cargo_deps="$pkgname-$pkgver
aho-corasick-0.7.3
autocfg-0.1.4
byteorder-1.3.2
cfg-if-0.1.9
chan-0.1.23
csv-1.0.7
csv-core-0.1.5
csv-index-0.1.5
docopt-1.1.0
filetime-0.1.15
fuchsia-cprng-0.1.1
itoa-0.4.4
lazy_static-1.3.0
libc-0.2.58
log-0.4.6
memchr-2.2.0
num-traits-0.2.8
num_cpus-1.10.1
proc-macro2-0.4.30
quickcheck-0.6.2
quote-0.6.12
rand-0.3.23
rand-0.4.6
rand_core-0.3.1
rand_core-0.4.0
rdrand-0.4.0
redox_syscall-0.1.54
regex-1.1.7
regex-syntax-0.6.7
ryu-0.2.8
serde-1.0.92
serde_derive-1.0.92
streaming-stats-0.2.2
strsim-0.9.2
syn-0.15.36
tabwriter-1.1.0
thread_local-0.3.6
threadpool-1.7.1
ucd-util-0.1.3
unicode-width-0.1.5
unicode-xid-0.1.0
utf8-ranges-1.0.3
winapi-0.3.7
winapi-i686-pc-windows-gnu-0.4.0
winapi-x86_64-pc-windows-gnu-0.4.0
"

source="$source $(echo $cargo_deps | sed -E 's#([[:graph:]]+)-([0-9.]+(-(alpha|beta|rc)[0-9.]+)?)#&.tar.gz::https://crates.io/api/v1/crates/\1/\2/download#g')"

prepare() {
	export CARGO_HOME="$srcdir/cargo-home"
	export CARGO_VENDOR="$CARGO_HOME/adelie"

	(builddir=$srcdir; default_prepare)

	mkdir -p "$CARGO_VENDOR"
	cat <<- EOF > "$CARGO_HOME/config"
		[source.adelie]
		directory = "${CARGO_VENDOR}"

		[source.crates-io]
		replace-with = "adelie"
		local-registry = "/nonexistant"
	EOF

	for _dep in $cargo_deps; do
		ln -s "$srcdir/$_dep" "$CARGO_VENDOR/$_dep"
		_sum=$(sha256sum "$srcdir/$_dep.tar.gz" | cut -d' ' -f1)
		cat <<- EOF > "$CARGO_VENDOR/$_dep/.cargo-checksum.json"
		{
			"package":"$_sum",
			"files":{}
		}
		EOF
	done
}

build() {
	export CARGO_HOME="$srcdir/cargo-home"
	export PKG_CONFIG_ALL_DYNAMIC=1
	export RUSTONIG_SYSTEM_LIBONIG=1
	cargo build -j $JOBS --release
}

check() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo test -j $JOBS --release
}

package() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo install --path . --root="$pkgdir"/usr
	rm "$pkgdir"/usr/.crates.toml
}

sha512sums="6db4e6e13613feb645bd71d9a1c9b7c9150e04f9880731bea143248ceb5c460503799d6f487f3c983f613964a56c998ba9d325cc1b61def8db0b63e74e387ce1  xsv-0.13.0.tar.gz
4250dc8fa38ad74e0c25375744f34eb06ff3bddbf16d6b4d757a9053ca16c3945d15c8f9deb11ea55d30b6d1c1744c4bccc388a4413b723a429445bf5ef3ffa9  aho-corasick-0.7.3.tar.gz
811b68ea24a836980026abba12598b35359abdff5660e6e9d3cc65e3edbedcd10dffc208900af5d4c21e983e1218b5fb5499117c05ab60b3e4716f0529b231ce  autocfg-0.1.4.tar.gz
2ef6b986926a4671dd945583730b0bfd4bd5e75b62a8a70c2875328157ba95f2c1b17c534d905e9b287457bd34363c1a33fd3dee9217c371032393ebbe206a8f  byteorder-1.3.2.tar.gz
45f7322217d291b3905ffdc45cadd5a7a7baf440f9a82a5b5596192ed0ac54353a3ecae0326d5807aae99bc4d79e0406d71bd65745ec8d9f8815a7c9436d648c  cfg-if-0.1.9.tar.gz
87c669ad5e5216c03bec9c32136af45e8dc5e1d43fa6fc06374d2ea7dea9b13f87458ca41c4bf2f615686a128664f3360b911c8f74c9091b70431b4d3adc31bc  chan-0.1.23.tar.gz
d3e69e0a93b55617b79a32e4429ea56eee03b5b01a9a62a132a41c0a4961db1bff418681ac3ebbfa30cb0c82ce2beeab2d9fa469a0514e5ff9ef64b969de4acb  csv-1.0.7.tar.gz
fc223ef33b83ae306f6cc5a77b08323a802f8fa39054ad9c4cffc63ba3195344319bb66471d164fa6eed7ef3bec11fb8308b78a4fe42ff91413e9441f021d439  csv-core-0.1.5.tar.gz
a1e928d3879d1490ade4f7172fe0cfd4221d28637109a65f856d277b88e1c7d823f447c82143d541c1b2b6fcec3050b8ea88b3dc9d9ef2e36b5ff97d84de66da  csv-index-0.1.5.tar.gz
f5dd97b4dbc9109811a83dba45bb09018f394adf3f5389cc62f7d42749c90dc337b7f5efdee569150a70194f5bda777a22107123ec7341a38c4f9923bb6f9f81  docopt-1.1.0.tar.gz
81d6e80f3da3ce12295ce8d85bf174eac3ee3dcc7d57e5a65f2ae91ab4f3dcf294582788af24db9ce2f18cb0e43080fb1baac76929dacec32d5ba4d09928b962  filetime-0.1.15.tar.gz
ea9f5beb0dfcb023c22cfc2b37ce52dfcf3a2cbbed0f79ffffc332878858386805c65dce8469a431002367562d857a6c064e075688540c27fcb4056a110059d2  fuchsia-cprng-0.1.1.tar.gz
f5e04bd908457e7592243ce64a99c5283428b767f4cc17d77946770411b06fccb0250625263c3e84a02a018ea7e8a0e4216e1929a71988bab8e1dbf603d3801d  itoa-0.4.4.tar.gz
08288790139876765b6d4a5988f47fd2a4bfc77c2f2406ad44e64920a471b5655c7f54cb197e5a40c29ee8b42aecbbefaac2b6f4a7dd2b5e24dd92c46cb9b822  lazy_static-1.3.0.tar.gz
9127ad9a94f75655740fc3a2278c7a17d5f03c4cd12c8833c1a1ecb7a860ec8581fbc969f5c2e23b8eecb9131d9d8271131ad6a675b785f18fb55d830bcc0491  libc-0.2.58.tar.gz
3844c771f97531ae7312c6bb0cf74ccaab94925f68bf1172419cc44fa4b5373f3ac6b42fb6d0ba636a970b295ea5d1b5abbe72b4da67a103e8dc4ed48a0cc5cb  log-0.4.6.tar.gz
19bb5fe9c4ff471e2f8cba0e3be3ebb715f0eaf3d9c012c05833e78243d8baa6b98e14eb4c9a900caca48aa539483972f4ff8e5a47aec5141f96f3f23fc24088  memchr-2.2.0.tar.gz
a3019e45744245837b2f146ab4213746c93a1ad701958dd418272af89451b843f9b643eac673c1397f6079da7709c7e9ff46e0e829a4cf6092772c9d77bbc534  num-traits-0.2.8.tar.gz
f1f7408dc1cfe7c718928d8e3e219e3001ce4207467a0a129546f2729ba43a7bf334cd5d328a7f8f8b1c276382c8a0f9d7abf60a3ae2c32f4adafa96c6ea62cd  num_cpus-1.10.1.tar.gz
73a8de3f1d76a8baf2d45afc1497bba8c0cbf231bf9b6750b9cee2473f492d5f3957ac149998da720acc8287c96d604971b51dcdfa629523bbdd97c297856ac0  proc-macro2-0.4.30.tar.gz
18483a244f83d64f298bcf6f71c137043dd1c28e22da6baf13760b2a39d77f84d2b7b651facb378b8559eb3d3a4cb53c16671128358e4a86e90e55e66d7ee41f  quickcheck-0.6.2.tar.gz
5d58945b0c9b41e368458a1661ae7b2ceed2e3285d377088afe5fde38e1db945ab42326d096cd6d13d82f5f80fe73d204c5fbe3c7abdea149eb6d7ca7c5e6798  quote-0.6.12.tar.gz
a4f46fb7dfe39677dfc112eeb56fb3c28cec4cdaaf95f336f2a494f0b2b30fdf22ceb9ea956f90e13c7ffe711a68aef9fc1a80d9a911d93221ae263c243fb2c3  rand-0.3.23.tar.gz
a91c6da7188b426bf9cb832892ee2af87c4cd65fad505c34e9c63343da71efe8c0c67b75c405dca5345317b7940d1d0fc0b20be85afd6b3089203e5698d86f0a  rand-0.4.6.tar.gz
5a7ae601124502bede760fd3179c2b28059ebc3b5983bfcb6b8fa62fb58df95cedc1aeb2734e792d894dfa4620801c13c29702f9cbee64243121575d4b6b9114  rand_core-0.3.1.tar.gz
f80e76dabd3308a12880a9aa8b7be83db39b02778c95bb63f862488789a2a67e2f08d4f2dd1ad803c61df0a9fc7f6620aa753b3bf394542ce27c89189a911845  rand_core-0.4.0.tar.gz
6476275d124bee28747191471e8d8f321a3b1c148c1f2a7ece4175f5244a7de90afe5f99d2eba5244d886b92e38232398864bf90e6d434b09494533942c8d894  rdrand-0.4.0.tar.gz
38ee15c2fa470428329b3888fef1f1b5bc57ffae96b6ec505fc051f33a8da86512afddfeb6966cb2342382a5cbccb624a825767d3492b3d6d21d6f8e97e57e9e  redox_syscall-0.1.54.tar.gz
17a06dc448c1be0910ddf5c03979fcd151d0a40acec5fedf96febe0f0fc52bda0252c40f8e7a18dcf3fa13f82f8a5b406b18f59e43713aec2adc2cae068f9120  regex-1.1.7.tar.gz
630e998b26ea7571591ff4259a5119d7762567253564b4a958cfd0f3f4de5857894e12eda469d940d8ed5572712b345e2859de282cdebf89a5108b97da71a073  regex-syntax-0.6.7.tar.gz
15a17fa06cb971847386013b7bc80e0483bb30f62062ac1e3200d588cb52771a7d34cdd74aba51de46341d303bd29065cf1f8cdcc17c23576cfadaefe63384b4  ryu-0.2.8.tar.gz
a8a33cf76a4f899205b2ede5e91859e03d038b178f685a36991c1836ab0c37f91ff50db4145e825838ac0fb4c526c9f496d00f87ac1790d101f4e83779fc5124  serde-1.0.92.tar.gz
37da456bb1392e4eacad07118dd8a6590552f455151f35bf4b350305772eddc8b232b61c36940c287df85b61ccc9867602cfc7012f7b7b645ab97cff45baeaab  serde_derive-1.0.92.tar.gz
520a8486f5d10b66a93e3b06e83ffb369bc0ee187fd53204d00076e62d402e37ab9fdcadc286723155a487f99324a64487cdea767b715d7494b82beba636d0fb  streaming-stats-0.2.2.tar.gz
2054444ea844a25f573219a4993986b69ff8c5dc0dfd2dd06fff638d596c8c5b5a1bedafc2bef3a1b9c146dd47a0769bbe039c54fd8e2a5d9894f29ab9f32838  strsim-0.9.2.tar.gz
7cd22f17d3740223d94b64405ba6283fb90a4ec170bd55de652a409a537b5355717dfb09dab991e1bbb799e57d1d48fc07c061adbe35f5b7da3bcdcdc8723ddf  syn-0.15.36.tar.gz
ed80e5e0d31f5941c63bf8f311b71919bf9fe84d872dcd31b37a1eac57a4efe4025b56784fd4083b952c7cae47f08562d10ed679cff0edb8b8dc57ff87ac7ee5  tabwriter-1.1.0.tar.gz
cd783d3d9caec43868da1f6118d4c4d520e03b9f1049d8f15d2c12482989401d3aee748e04a149953d35e5d6487355c2891d44569ef688bc1d45f01b6461d253  thread_local-0.3.6.tar.gz
e46733ea55a27a32868596562bbaf1e50508dea58359006cabd160b06cec4854c97b5d253cdcb8bfd60c7b3a33e496bec4159d0621efaa3cdcbc77255be4f275  threadpool-1.7.1.tar.gz
50a796feb198012241d73001bb53c8e3d3168df379de91d7ecaccfd829b08a356afe3db736fb8cced8748141d7a9e4b157440442c425816c78c0615c08e20f2e  ucd-util-0.1.3.tar.gz
bd5ac5f0433953d79408074239edc7c43ce23d56659d467805d81ab01c576a3cf77ccedb3bba41d48bc4ad46a8905ac8a1927b99312053ef6295fd940a6766d2  unicode-width-0.1.5.tar.gz
cc5343e2166938322cfd7c73f1f918f2a9c46846ac0ef55933d1e44cdfaf6f7da2b7ff18b68e356c47b6d8ba5565eda0db42c347dcbde830683f341ac2b1849d  unicode-xid-0.1.0.tar.gz
24907ad7ae1a02713e6ecc62e0c73488abea338f0dd3b49291b914ca907b3a220cb90f8ca409c6aa57d2e0e5d8ca8c44cd310081ffe7be9208952d73ec53b9f8  utf8-ranges-1.0.3.tar.gz
6871b93ad8d48e39b90cb7b31b3132f84665f965b4dfe06fcebdfb873e7d099007cf3d7a50e832a941c3425ad2f39c3ab48a77151e60863685b97fc05c71d134  winapi-0.3.7.tar.gz
a672ccefd0730a8166fef1d4e39f9034d9ae426a3f5e28d1f4169fa5c5790767693f281d890e7804773b34acdb0ae1febac33cde8c50c0044a5a6152c7209ec2  winapi-i686-pc-windows-gnu-0.4.0.tar.gz
4a654af6a5d649dc87e00497245096b35a2894ae66f155cb62389902c3b93ddcc5cf7d0d8b9dd97b291d2d80bc686af2298e80abef6ac69883f4a54e79712513  winapi-x86_64-pc-windows-gnu-0.4.0.tar.gz"
