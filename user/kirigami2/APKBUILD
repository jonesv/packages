# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kirigami2
pkgver=5.68.0
pkgrel=0
pkgdesc="Framework for rapidly designing usable interfaces"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require accelerated X11 desktop.
license="LGPL-2.1+"
depends="qt5-qtgraphicaleffects"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev
	qt5-qtquickcontrols2-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kirigami2-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="f9f84893dec3298066e7dd3f283a118a488403d23895992875b9229d26d3b3ad14ff14bdd7a5afc79591d96ac431556ea15fcb65dffe23c42a3144f1fbe88f21  kirigami2-5.68.0.tar.xz"
