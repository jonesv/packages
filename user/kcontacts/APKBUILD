# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcontacts
pkgver=5.68.0
pkgrel=0
pkgdesc="Library for working with contact information"
url="https://www.kde.org"
arch="all"
license="LGPL-2.1-only"
depends="iso-codes"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev kcoreaddons-dev
	ki18n-dev kcodecs-dev iso-codes-dev doxygen graphviz qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcontacts-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# addresstest requires the library to already be installed.
	# picturetest requires X11 running.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(address|picture)test"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="59734f680473ba97bae3406c04f9859997ddb29233d1870865c7f147b7271b7a3dde6ed05bf4cc8ebabd7639c8492bae1f8bab38437c3947495f3bc50190341f  kcontacts-5.68.0.tar.xz"
