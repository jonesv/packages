# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivities-stats
pkgver=5.68.0
pkgrel=0
pkgdesc="Gather statistics about KDE activities"
url="https://api.kde.org/frameworks/kactivities/html/index.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="kactivities-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev boost-dev
	qt5-qtdeclarative-dev qt5-qttools-dev doxygen graphviz kconfig-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-stats-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="ba4241d0aa0b76639d7d7231ef3cf6f38db710dc6df8d9553242c1faac40839853437710cfcfb1f246c68e7e26a4fe887eb5b058421798e885b43777b0360be9  kactivities-stats-5.68.0.tar.xz"
