# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kholidays
pkgver=5.68.0
pkgrel=0
pkgdesc="List of national holidays for many countries"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# Requires *actual* *locale* *support*!
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E testholidayregion
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ad4ab73880862104cc7aa1fff2dd91c5b9fe36f8f8ee0fb1f217a0e159ca4184984767ba5bebbb3ef0bd6a1fd6cbff09a019fe3c26826ab4df5cfabcb80ce9a3  kholidays-5.68.0.tar.xz"
