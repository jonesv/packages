# Contributor: Travis Tilley <ttilley@gmail.com>
# Contributor: Mitch Tishmack <mitch.tishmack@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
_pkgname=llvm
pkgver=8.0.1
_majorver=${pkgver%%.*}
pkgname=$_pkgname$_majorver
pkgrel=0
pkgdesc="Low Level Virtual Machine compiler system, version $_majorver"
arch="all"
options="!checkroot !dbg"
url="https://llvm.org/"
license="NCSA"
depends_dev="$pkgname=$pkgver-r$pkgrel libexecinfo-dev libxml2-dev"
makedepends="binutils-dev chrpath cmake file libexecinfo-dev libffi-dev
	libxml2-dev python3 zlib-dev"
subpackages="$pkgname-static $pkgname-libs $pkgname-dev
	$pkgname-test-utils:_test_utils"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-$pkgver.src.tar.xz
	llvm-fix-build-with-musl-libc.patch
	disable-FileSystemTest.CreateDir-perms-assert.patch
	disable-dlclose-test.patch
	musl-ppc64-elfv2.patch
	more-secure-plt.patch
	even-more-secure-plt.patch
	python3-test.patch
	"
builddir="$srcdir/$_pkgname-$pkgver.src"

# ARM has few failures in test suite that we don't care about currently and
# also it takes forever to run them on the builder.
case "$CARCH" in
	arm*) options="$options !check";;
esac

# Whether is this package the default (latest) LLVM version.
_default_llvm="yes"

if [ "$_default_llvm" = yes ]; then
	provides="llvm=$pkgver-r$pkgrel"
	replaces="llvm"
fi

# NOTE: It seems that there's no (sane) way how to change includedir, sharedir
# etc. separately, just the CMAKE_INSTALL_PREFIX. Standard CMake variables and
# even  LLVM-specific variables, that are related to these paths, actually
# don't work (in llvm 3.7).
#
# When building a software that depends on LLVM, utility llvm-config should be
# used to discover where is LLVM installed. It provides options to print
# path of bindir, includedir, and libdir separately, but in its source, all
# these paths are actually hard-coded against INSTALL_PREFIX. We can patch it
# and move paths manually, but I'm really not sure what it may break...
#
# Also note that we should *not* add version suffix to files in llvm bindir!
# It breaks build system of some software that depends on LLVM, because they
# don't expect these files to have a sufix.
#
# So, we install all the LLVM files into /usr/lib/llvm$_majorver.
# BTW, Fedora and Debian do the same thing.
#
_prefix="usr/lib/llvm$_majorver"

prepare() {
	default_prepare

	# https://bugs.llvm.org//show_bug.cgi?id=31870
	rm test/tools/llvm-symbolizer/print_context.c
}

build() {
	# Auto-detect it by guessing either.
	local ffi_include_dir="$(pkg-config --cflags-only-I libffi | sed 's|^-I||g')"

	cmake -G "Unix Makefiles" -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/$_prefix \
		-DCMAKE_VERBOSE_MAKEFILE=NO \
		-DFFI_INCLUDE_DIR="$ffi_include_dir" \
		-DLLVM_BINUTILS_INCDIR=/usr/include \
		-DLLVM_BUILD_DOCS=OFF \
		-DLLVM_BUILD_EXAMPLES=OFF \
		-DLLVM_BUILD_EXTERNAL_COMPILER_RT=ON \
		-DLLVM_BUILD_LLVM_DYLIB=ON \
		-DLLVM_BUILD_TESTS=ON \
		-DLLVM_DEFAULT_TARGET_TRIPLE="$CBUILD" \
		-DLLVM_ENABLE_ASSERTIONS=OFF \
		-DLLVM_ENABLE_CXX1Y=ON \
		-DLLVM_ENABLE_FFI=ON \
		-DLLVM_ENABLE_LIBCXX=OFF \
		-DLLVM_ENABLE_PIC=ON \
		-DLLVM_ENABLE_RTTI=ON \
		-DLLVM_ENABLE_SPHINX=OFF \
		-DLLVM_ENABLE_TERMINFO=ON \
		-DLLVM_ENABLE_ZLIB=ON \
		-DLLVM_HOST_TRIPLE="$CHOST" \
		-DLLVM_INCLUDE_EXAMPLES=OFF \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DLLVM_TARGETS_TO_BUILD='AArch64;AMDGPU;ARM;BPF;Hexagon;Lanai;Mips;MSP430;PowerPC;Sparc;SystemZ;WebAssembly;X86;XCore' \
		-Bbuild .

	make -C build llvm-tblgen
	make -C build

	python3 utils/lit/setup.py build
}

check() {
	# appears to be an issue on musl and glibc, but only fails on musl:
	# https://github.com/NixOS/nixpkgs/blob/bb7e9e46/pkgs/development/compilers/llvm/8/llvm.nix#L74
	rm -f "$builddir"/test/CodeGen/AArch64/wineh4.mir
	make -C build check-llvm
}

package() {
	make DESTDIR="$pkgdir" -C build install

	cd "$pkgdir"/$_prefix

	# Remove RPATHs.
	file lib/*.so bin/* \
		| awk -F: '$2~/ELF/{print $1}' \
		| xargs -r chrpath -d

	# Symlink files from /usr/lib/llvm*/bin to /usr/bin.
	mkdir -p "$pkgdir"/usr/bin
	local name newname path
	for path in bin/*; do
		name=${path##*/}
		# Add version infix/suffix to the executable name.
		case "$name" in
			llvm-*) newname="llvm$_majorver-${name#llvm-}";;
			*) newname="$name$_majorver";;
		esac
		# If this package provides=llvm (i.e. it's the default/latest
		# llvm package), omit version infix/suffix.
		if [ "$_default_llvm" = yes ]; then
			newname=$name
		fi
		case "$name" in
			FileCheck | obj2yaml | yaml2obj) continue;;
		esac
		ln -s ../lib/llvm$_majorver/bin/$name "$pkgdir"/usr/bin/$newname
	done

	# Move /usr/lib/$pkgname/include/ into /usr/include/$pkgname/
	# and symlink it back.
	_mv include/* "$pkgdir"/usr/include/$pkgname/
	rmdir include
	ln -s ../../include/$pkgname include

	ln -s "$pkgdir"/usr/lib/cmake/llvm ../$pkgname/lib/cmake/llvm
}

static() {
	pkgdesc="LLVM $_majorver static libraries"
	_common_subpkg

	_mv "$pkgdir"/$_prefix/lib/*.a "$subpkgdir"/$_prefix/lib/
	strip -d "$subpkgdir"/$_prefix/lib/*.a
}

libs() {
	pkgdesc="LLVM $_majorver runtime library"
	local soname="libLLVM-$_majorver.so"
	local soname2="libLLVM-$pkgver.so"
	_common_subpkg

	mkdir -p "$subpkgdir"
	cd "$subpkgdir"

	# libLLVM should be in /usr/lib. This is needed for binaries that are
	# dynamically linked with libLLVM, so they can find it on default path.
	_mv "$pkgdir"/$_prefix/lib/$soname usr/lib/
	ln -s $soname usr/lib/$soname2

	# And also symlink it back to the LLVM prefix.
	mkdir -p $_prefix/lib
	ln -s ../../$soname $_prefix/lib/$soname
	ln -s ../../$soname $_prefix/lib/$soname2
}

dev() {
	_common_subpkg
	default_dev
	cd "$subpkgdir"

	_mv "$pkgdir"/$_prefix/lib $_prefix/
	_mv "$pkgdir"/$_prefix/include $_prefix/

	_mv "$pkgdir"/$_prefix/bin/llvm-config $_prefix/bin/
}

_test_utils() {
	pkgdesc="LLVM $_majorver utilities for executing LLVM and Clang style test suites"
	depends="python3"
	_common_subpkg
	replaces=""

	local litver="$(python3 "$builddir"/utils/lit/setup.py --version 2>/dev/null \
		| sed 's/\.dev.*$//')"
	test -n "$litver" || return 1
	provides="$provides lit=$litver-r$pkgrel"

	cd "$builddir"/build

	install -D -m 755 bin/FileCheck "$subpkgdir"/$_prefix/bin/FileCheck
	install -D -m 755 bin/count "$subpkgdir"/$_prefix/bin/count
	install -D -m 755 bin/not "$subpkgdir"/$_prefix/bin/not

	python3 ../utils/lit/setup.py install --prefix=/usr --root="$subpkgdir"
	ln -s ../../../bin/lit "$subpkgdir"/$_prefix/bin/lit
}

_common_subpkg() {
	if [ "$_default_llvm" = yes ]; then
		replaces="llvm${subpkgname#$pkgname}"
		provides="$replaces=$pkgver-r$pkgrel"
	fi
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv $@
}

sha512sums="82e120be5cabdfd5111aebbea68a663fe229c8861d73802d6ab09a3bf48f60de333e07e61f8fb61beaa14ac2bea24fcd74fa6f761acaf62469f536b79fcb1e16  llvm-8.0.1.src.tar.xz
f84cd65d7042e89826ba6e8d48c4c302bf4980da369d7f19a55f217e51c00ca8ed178d453df3a3cee76598a7cecb94aed0775a6d24fe73266f82749913fc3e71  llvm-fix-build-with-musl-libc.patch
49c47f125014b60d0ea7870f981a2c1708ad705793f89287ed846ee881a837a4dc0170bf467e03f2ef56177473128945287749ac80dc2d13cfabcf8b929ba58a  disable-FileSystemTest.CreateDir-perms-assert.patch
caeec8e4dbd92f5f74940780b69075f3879a267a8623822cbdc193fd14706eb089071e3a5a20d60cc2eca59e4c5b2a61d29827a2f3362ee7c5f74f11d9ace200  disable-dlclose-test.patch
e5ddbc4b6c4928e79846dc3c022eb7928aaa8fed40515c78f5f03b8ab8264f34f1eb8aa8bfc0f436450932f4917e54ad261603032092ea271d9590f11a37cf1e  musl-ppc64-elfv2.patch
7ba7f5b396e1afb49ea53fdc16729f0709fbba88de433cc8a8e2f751d13733011d4121318f68d7f8a16a6c57c3a1bee727cc3e0da0f5c6cae38eff70d3a539cf  more-secure-plt.patch
deb71762721ebc73bfdf23143b582f40c70eddcef3e337ed14499e8e336bee2906292d38d64fe98fa633430c1bcb66cf6a2e067258c8fbe6e931f99f6d10a6f7  even-more-secure-plt.patch
53cc0d13dd871e9b775bb4e7567de4f9a97d91b8246cd7ce74607fd88d6e3e2ab9455f5b4195bc7f9dbdedbc77d659d43e98ec0b7cd78cd395aaea6919510287  python3-test.patch"
