# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesu
pkgver=5.68.0
pkgrel=0
pkgdesc="Framework for elevating privileges"
url="https://api.kde.org/frameworks/kdesu/html/index.html"
arch="all"
options="suid"  # obvious
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev kpty-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev kcoreaddons-dev ki18n-dev kservice-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdesu-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="cd31b83be237adb2b13896714ee07af659780b1028149e04ce797edf737d03dd0a7e1f2733c2ae28786c38e382806b558725f1f424910747df13719d35fad5f2  kdesu-5.68.0.tar.xz"
