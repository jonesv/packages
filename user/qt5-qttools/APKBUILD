# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qttools
_pkgname=qttools-everywhere-src
pkgver=5.12.6
pkgrel=0
pkgdesc="Tools for Qt development"
url="https://www.qt.io/"
arch="all"
options="!check"  # Tests require KDE 4 with special window manager config
license="LGPL-3.0-only WITH Qt-LGPL-exception-1.1 OR GPL-3.0-only WITH Qt-GPL-exception-1.0"
depends=""
makedepends="qt5-qtbase-dev"
subpackages="$pkgname-dev qt5-assistant:_assistant qt5-qdbusviewer:_dbusviewer
	qdbus qtpaths"
source="https://download.qt.io/official_releases/qt/${pkgver%.*}/$pkgver/submodules/$_pkgname-$pkgver.tar.xz
	assistant-qt5.desktop
	designer-qt5.desktop
	linguist-qt5.desktop
	qdbusviewer-qt5.desktop
	"

_qt5_prefix=/usr/lib/qt5
builddir="$srcdir"/$_pkgname-$pkgver

build() {
	qmake
	make
}

package() {
	make install INSTALL_ROOT="$pkgdir"

	mkdir -p "$pkgdir"/usr/bin/
	for i in "$pkgdir"/$_qt5_prefix/bin/*; do
		ln -s ../lib/qt5/bin/${i##*/} "$pkgdir"/usr/bin/${i##*/}-qt5
		ln -s ../lib/qt5/bin/${i##*/} "$pkgdir"/usr/bin/${i##*/}
	done

	for i in $source; do
		case $i in
		*.desktop) install -Dm644 "$srcdir"/$i \
			"$pkgdir"/usr/share/applications/$i;;
		esac
	done
	# icons
	install -m644 -p -D src/assistant/assistant/images/assistant.png \
		"$pkgdir"/usr/share/icons/hicolor/32x32/apps/assistant-qt5.png
	install -m644 -p -D src/assistant/assistant/images/assistant-128.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/assistant-qt5.png
	install -m644 -p -D src/designer/src/designer/images/designer.png \
		"$pkgdir"/usr/share/icons/hicolor/32x32/apps/designer-qt5.png
	install -m644 -p -D src/qdbus/qdbusviewer/images/qdbusviewer.png \
		"$pkgdir"/usr/share/icons/hicolor/32x32/apps/qdbusviewer-qt5.png
	install -m644 -p -D src/qdbus/qdbusviewer/images/qdbusviewer-128.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/qdbusviewer-qt5.png
	# linguist icons
	for icon in src/linguist/linguist/images/icons/linguist-*-32.png ; do
		size=$(echo ${icon##*/} | cut -d- -f2)
		install -p -m644 -D ${icon} \
			"$pkgdir"/usr/share/icons/hicolor/${size}x${size}/apps/linguist.png
	done
}

_mv_files() {
	for i in "$@"; do
		mkdir -p "$subpkgdir"/${i%/*}
		mv "$pkgdir"/$i "$subpkgdir"/${i%/*}
	done
}

qdbus() {
	pkgdesc="Qt 5 D-Bus tool"
	depends="dbus-x11"
	_mv_files $_qt5_prefix/bin/qdbus usr/bin/qdbus-qt5 usr/bin/qdbus
}

qtpaths() {
	pkgdesc="Qt 5 path determination tool"
	_mv_files $_qt5_prefix/bin/qtpaths usr/bin/qtpaths-qt5 usr/bin/qtpaths
}

dev() {
	default_dev
	pkgdesc="More Qt 5 development utilities (Qt Designer, Qt Linguist)"
	for i in designer lconvert linguist lrelease lupdate pixeltool \
		qcollectiongenerator qhelpgenerator; do

		_mv_files $_qt5_prefix/bin/$i usr/bin/$i-qt5 usr/bin/$i
	done
	_mv_files \
		usr/share/applications/designer* \
		usr/share/applications/linguist* \
		usr/share/icons/hicolor/*/apps/designer* \
		usr/share/icons/hicolor/*/apps/linguist*
}

_assistant() {
	pkgdesc="Documentation browser for Qt 5"
	# workaround for weird shell bug???
	_mv_files usr/bin/assist* \
		$_qt5_prefix/bin/assist* \
		usr/share/applications/*assistant* \
		usr/share/icons/hicolor/{32x32,128x128}/apps/assistant*
}

_dbusviewer() {
	pkgdesc="Qt 5 D-Bus debugger and viewer"
	_mv_files usr/bin/qdbusviewer* \
		$_qt5_prefix/bin/qdbusviewer* \
		usr/share/applications/qdbusviewer* \
		usr/share/icons/hicolor/{32x32,128x128}/apps/qdbusviewer*
}

sha512sums="fd35a9dad9f797657121ce9daf916483f81fcc9dc2cd8ee30333d3614a17376579278f993ed2b96c578c64c7cdad0003151ed350c129263d377e0f1b0a34f787  qttools-everywhere-src-5.12.6.tar.xz
d566c5284854855541df7177b23f491d96f5064b571e899a44f1d4fcf8bbf1223590b05b1954278dc6f3f56341c917f5b846594c5bd2215b6a859224038d8ad2  assistant-qt5.desktop
72d9a2235a60c4ae05ba8395d473fe0b42c12e584da619dadb112eb67ba33a85fe0dab6c185d98112d7b25d3eeacaf02f7ef4731742e50c17eacc54c383661b3  designer-qt5.desktop
b6d8a672d19eed39ab868ff6fc880f255da94acb9e1e84f5905c0f3b9b6055547e8a706492973692c06dc23d35ce77622fc13efc11adf21b62c3baf4ef5ab2ad  linguist-qt5.desktop
dddcc8c945dcbaf834e8aa8f42fd6df8d6e257e673a256cfd4affed7caf119502dffe6864262d353e8c2e234296cd091279f120ab9502f1b323e20ae3c3dc709  qdbusviewer-qt5.desktop"
