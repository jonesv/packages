# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwallet
pkgver=5.68.0
pkgrel=0
pkgdesc="Secure storage system for passwords built atop Qt"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kconfig-dev kwindowsystem-dev
	ki18n-dev kconfigwidgets-dev kdbusaddons-dev kiconthemes-dev
	knotifications-dev kservice-dev libgcrypt-dev gpgme-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev doxygen
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwallet-$pkgver.tar.xz
	kwallet-5.22.0-blowfish-endianness.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a0a410a9040cb8f157ff01cfbc976f49dac90d0fda0b4dbc8e760cb0549073ed9be5d442bde03b4f6bf27aa16a397671a1a0285c179aea3760bbf441e22cf004  kwallet-5.68.0.tar.xz
edca12963a5db9db05b3b4d581c1b970569f3b96dc672422e561c189c9024b69710732281f054514ce3d596688a5b0ba512766f4fd768eea8e00a18dcfd59179  kwallet-5.22.0-blowfish-endianness.patch"
