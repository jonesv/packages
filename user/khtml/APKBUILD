# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khtml
pkgver=5.68.0
pkgrel=0
pkgdesc="The KDE HTML library, ancestor of WebKit"
url="https://konqueror.org/"
arch="all"
options="!check"  # Tests require X11.
license="LGPL-2.1+ AND LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev kcodecs-dev ki18n-dev kparts-dev ktextwidgets-dev
	kjs-dev"
makedepends="$depends_dev cmake extra-cmake-modules giflib-dev gperf kio-dev
	karchive-dev kglobalaccel-dev kiconthemes-dev knotifications-dev
	kwallet-dev libjpeg-turbo-dev libpng-dev libx11-dev openssl-dev
	phonon-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a306f5da7bf03f77111af4f3e4d4a2b21e483861e7e5d4265941e914f788e58079fce11ab2667bb802d512d7c84fa70653a94a16cdb14221ce1aebbe300ce2ee  khtml-5.68.0.tar.xz"
