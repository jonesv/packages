# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=cifs-utils
pkgver=6.10
pkgrel=0
pkgdesc="CIFS filesystem user-space tools"
url="https://wiki.samba.org/index.php/LinuxCIFS_utils"
arch="all"
options="!check suid"  # No test suite.
license="GPL-3.0+ AND GPL-2.0+ AND LGPL-3.0+"
depends=""
makedepends="keyutils-dev krb5-dev libcap-ng-dev linux-pam-dev py3-docutils
	talloc-dev autoconf automake"
subpackages="$pkgname-doc $pkgname-dev"
source="https://ftp.samba.org/pub/linux-cifs/$pkgname/$pkgname-$pkgver.tar.bz2
	musl-fix-includes.patch
	respect-destdir.patch
	xattr_size_max.patch
	"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	# --enable-cifsidmap and --enable-cifsacl require libwbclient (samba)
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-cifsidmap=no \
		--enable-cifsacl=no
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	# Allow unprivileged mount
	chmod u+s "$pkgdir"/sbin/mount.cifs
}

sha512sums="e19ca69b7948f01c1fd6a4ed069e00511588b903a5b8b0dc35ac1e00743170b9ca180b747c47d56cfacf273b296da21df60e1957404f26ebf2ba80bfa7e275cc  cifs-utils-6.10.tar.bz2
99a2fab05bc2f14a600f89526ae0ed2c183cfa179fe386cb327075f710aee3aed5ae823f7c2f51913d1217c2371990d6d4609fdb8d80288bd3a6139df3c8aebe  musl-fix-includes.patch
f3acb4f7873628d67c7dfb2378135c302fe382e314277829ea5569710bac0ddb43684aa6d143327d735aec641997084eaa567823b534138ed884bd74044b652a  respect-destdir.patch
2a9366ec1ddb0389c535d2fa889f63287cb8374535a47232de102c7e50b6874f67a3d5ef3318df23733300fd8459c7ec4b11f3211508aca7800b756119308e98  xattr_size_max.patch"
