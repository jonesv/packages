# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pidgin
pkgver=2.13.0
pkgrel=1
pkgdesc="Multi-protocol instant messaging client"
url="http://pidgin.im/"
arch="all"
license="GPL-2.0-only"
depends="gst-plugins-base"
makedepends="gtk+2.0-dev perl-dev libsm-dev startup-notification-dev
	libxml2-dev libidn-dev gnutls-dev dbus-dev dbus-glib-dev gstreamer-dev
	cyrus-sasl-dev ncurses-dev nss-dev tcl-dev tk-dev intltool gtkspell-dev
	gst-plugins-base-dev farstream-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang finch libpurple"
source="https://bitbucket.org/pidgin/main/downloads/$pkgname-$pkgver.tar.bz2"

build() {
	LIBS="-lX11 -ltinfo" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-avahi \
		--enable-dbus \
		--disable-doxygen \
		--enable-gnutls \
		--disable-meanwhile \
		--disable-nm \
		--enable-perl \
		--disable-python \
		--disable-schemas-install \
		--disable-screensaver \
		--enable-tcl \
		--enable-vv \
		--enable-gstreamer \
		--enable-gstreamer-interfaces \
		--enable-farstream \
		--enable-cyrus-sasl \
		--enable-nss
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

finch() {
	pkgdesc="Text-based multi-protocol instant messaging client"
	mkdir -p "$subpkgdir"/usr/lib "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/gnt "$pkgdir"/usr/lib/libgnt.so.* \
		"$pkgdir"/usr/lib/finch \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/finch "$subpkgdir"/usr/bin/
}

libpurple() {
	pkgdesc="Multi-protocol instant messaging library"
	mkdir -p "$subpkgdir"/usr/lib "$subpkgdir"/usr/share/sounds
	mv "$pkgdir"/usr/lib/*purple* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/purple "$pkgdir"/usr/share/sounds \
		"$subpkgdir"/usr/share/
}

sha512sums="68b3d1eefee111544c7eb347386d0aea4f47c3e320d5963a4e0d833ed6af7b1be243a7bcd6a38c9234b58601d10a9aebf8541f1d97decfeca754fa78dc693047  pidgin-2.13.0.tar.bz2"
