# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=farstream
pkgver=0.2.9
pkgrel=0
pkgdesc="Audio/Video communications framework"
url="https://www.freedesktop.org/wiki/Software/Farstream/"
arch="all"
options="!check"  # Tests are really really brittle and depend on networking.
license="LGPL-2.1+"
depends="gst-plugins-base gst-plugins-good"
depends_dev="glib-dev"
makedepends="$depends_dev gobject-introspection-dev gstreamer-dev
	gst-plugins-base-dev libnice-dev autoconf automake libtool cmd:which"
subpackages="$pkgname-dev $pkgname-doc"
source="https://freedesktop.org/software/farstream/releases/farstream/farstream-$pkgver.tar.gz
	make43.patch
	"

build() {
	./autogen.sh --disable-gtk-doc
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-package-origin="${DISTRO_NAME-:Adélie Linux} (${DISTRO_URL-:https://www.adelielinux.org/})"
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8840725d2f1de59bebc8c49cf68a6712822a4e7b6ba9078713f80e3c35a39135565a1a885edab43a562f895e11b8f5d8d666d57d9b3814dabb661f46498515bd  farstream-0.2.9.tar.gz
ea863d7e72e6c0311c1e6f4718721f4f03955e0c9a61066db833cc0b73246b27387067abfe677f594c6a132c7e3836de50cc216903ba12e286638988f41e3bd5  make43.patch"
