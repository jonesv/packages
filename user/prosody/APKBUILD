# Contributor: Mika Havela <mika.havela@gmail.com>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=prosody
pkgver=0.11.5
pkgrel=0
pkgdesc="Lua based Jabber/XMPP server"
url="http://prosody.im/"
arch="all"
options="!check" # broken testsuite
license="MIT"
depends="lua-socket lua-expat lua-filesystem lua-sec lua5.3"
makedepends="linux-headers lua5.3-dev libidn-dev openssl-dev"
install="prosody.pre-install"
subpackages="$pkgname-doc $pkgname-openrc"
pkgusers="prosody"
pkggroups="prosody"
source="https://prosody.im/downloads/source/$pkgname-$pkgver.tar.gz
	prosody.cfg.lua.patch
	$pkgname.initd
	"

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc/prosody \
		--ostype=linux \
		--with-lua-lib=/usr/lib \
		--with-lua-include=/usr/include \
		--lua-version=5.3 \
		--no-example-certs
	# Don't generate certs
	rm -f "$builddir"/certs/Makefile

	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -d -o prosody -g prosody "$pkgdir/var/log/prosody"
	install -d -o prosody -g prosody "$pkgdir/var/run/prosody"
	install -d -m750 -o prosody -g prosody "$pkgdir/var/lib/prosody"

	install -D -m755 "$srcdir"/"$pkgname".initd "$pkgdir"/etc/init.d/"$pkgname"
}

sha512sums="43a9facee8d61b69e277e7e8a9091fbfa60be71d6141195dff991f6020c8afadacce26f356b869cb2d7edd9be285e9ce1bcd822f9628cc845d91757e818a0dff  prosody-0.11.5.tar.gz
a6ca168fe3d11ee3b05295fb36dfaf8240c60a85507032b2502f9a97d3fd055f7eee38ba6efbb8f79472fc7cdd3556922194d0bd7099f7fb809be01890acc511  prosody.cfg.lua.patch
11b0f5e4fa488e047c26aa5e51c35983100cdbf7ebbf7c8b6d003c8db7f52e797f93e4744d54b3094c82d722d5e4de62b5734376cb5e69a4c6127f8cb07a4347  prosody.initd"
