# Contributor: Fabio Riga <rifabio@dpersonam.me>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sane
_pkgname=sane-backends
pkgver=1.0.29
pkgrel=0
pkgdesc="Scanner access library"
url="http://www.sane-project.org/"
arch="all"
license="GPL-2.0+ AND GPL-2.0+-with-sane-exception AND Public-Domain"
depends=""
makedepends="diffutils file libtool libusb-dev v4l-utils-dev net-snmp-dev
	libpng-dev libjpeg-turbo-dev tiff-dev libgphoto2-dev libieee1284-dev
	linux-headers ncurses-dev"
install="saned.pre-install $pkgname.pre-install"
pkgusers="saned"
pkggroups="scanner"
_backends="abaton agfafocus apple artec artec_eplus48u as6e avision bh canon
	canon630u canon_dr canon_pp cardscan coolscan coolscan2 coolscan3 dc25
	dc210 dc240 dell1600n_net dmc epjitsu epson epson2 epsonds fujitsu
	genesys gphoto2 gt68xx hp hp3500 hp3900 hp4200 hp5400 hp5590 hpsj5s
	hpljm1005 hs2p ibm kodak kodakaio kvs1025 kvs20xx leo lexmark ma1509
	magicolor matsushita microtek microtek2 mustek mustek_pp mustek_usb
	nec net niash p5 pie pieusb pixma plustek plustek_pp ricoh ricoh2
	rts8891 s9036 sceptre sharp sm3600 sm3840 snapscan sp15c st400 stv680
	tamarack teco1 teco2 teco3 test u12 umax umax_pp umax1220u v4l
	xerox_mfp"
case "$CARCH" in
x86*) _backends="$_backends qcam";;
esac

_pkgdesc_dell1600n_net="SANE backend for Dell 1600n (Ethernet only; USB not supported)"
for _backend in $_backends; do
	subpackages="$subpackages $pkgname-backend-$_backend:_backend"
done
subpackages="$pkgname-doc $pkgname-dev $subpackages $pkgname-utils saned
	saned-openrc:openrc:noarch $pkgname-udev::noarch $_pkgname::noarch
	$pkgname-lang"
source="https://gitlab.com/sane-project/backends/uploads/54f858b20a364fc35d820df935a86478/sane-backends-1.0.29.tar.gz
	saned.initd
	endian.patch
	include.patch
	network.patch
	pidfile.patch
	check.patch
	"
builddir="$srcdir"/$_pkgname-$pkgver

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--with-usb \
		--disable-rpath \
		--disable-locking
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	printf "" > "$pkgdir"/etc/sane.d/dll.conf
	install -Dm644 backend/dll.aliases "$pkgdir"/etc/sane.d/dll.aliases
}

doc() {
	default_doc
	mkdir -p "$subpkgdir"/usr/share/licenses/$_pkgname
	mv "$subpkgdir"/usr/share/doc/$_pkgname/LICENSE \
		"$subpkgdir"/usr/share/licenses/$_pkgname
}

saned() {
	pkgdesc="Network scanner server"
	mkdir -p "$subpkgdir"/etc/sane.d "$subpkgdir"/usr
	mv "$pkgdir"/etc/sane.d/saned.conf "$subpkgdir"/etc/sane.d
	mv "$pkgdir"/usr/sbin "$subpkgdir"/usr/
}

openrc() {
	pkgdesc="Network scanner server (OpenRC runscripts)"
	depends="saned"
	install_if="saned=$pkgver-r$pkgrel openrc"
	install -Dm755 "$srcdir"/saned.initd "$subpkgdir"/etc/init.d/saned
}

utils() {
	pkgdesc="$pkgdesc (utilities)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

udev() {
	pkgdesc="$pkgdesc (udev rules)"
	install_if="$pkgname=$pkgver-r$pkgrel udev"
	install -Dm644 "$builddir"/tools/udev/libsane.rules \
		"$subpkgdir"/usr/lib/udev/rules.d/49-sane.rules
	sed -i 's|NAME="%k", ||g' "$subpkgdir"/usr/lib/udev/rules.d/49-sane.rules
}

backends() {
	local _backend;
	pkgdesc="$pkgdesc (metapackage)"
	depends="$pkgname-utils saned"
	for _backend in $_backends; do
		[ "$_backend" = "test" ] && continue
		depends="$depends $pkgname-backend-$_backend"
	done
	mkdir -p "$subpkgdir"
}

_backend() {
	local name="${subpkgname#$pkgname-backend-}"
	depends="$pkgname"
	pkgdesc=$(eval echo \$_pkgdesc_$name)
	if [ ! "$pkgdesc" ]; then
		# cut description from man-page
		pkgdesc=$(tr '\n' ' ' < "$builddir"/doc/$pkgname-$name.man)
		pkgdesc=${pkgdesc#*\- }
		pkgdesc=${pkgdesc%% .SH *};
	fi
	mkdir -p "$subpkgdir"/usr/lib/$pkgname \
		"$subpkgdir"/etc/$pkgname.d/dll.d
	mv "$pkgdir"/usr/lib/sane/lib$pkgname-$name.* \
		"$subpkgdir"/usr/lib/sane
	echo "$name" > "$subpkgdir"/etc/sane.d/dll.d/$name
	if [ -f "$pkgdir"/etc/sane.d/$name.conf ]; then
		mv "$pkgdir"/etc/sane.d/$name.conf \
			"$subpkgdir"/etc/sane.d
	fi
	if [ -f "$pkgdir"/usr/bin/$name ]; then
		mkdir -p "$subpkgdir"/usr/bin
		mv "$pkgdir"/usr/bin/$name "$subpkgdir"/usr/bin
	fi
}

sha512sums="9ee431b0c048119719e15905f8743c7a0fd8bc6fcee81d75df8459a989cab7e39bf045518976713a7e1d94c816dd1a1e0fa190612d815c0ddb6a51d2abf3fd54  sane-backends-1.0.29.tar.gz
0a06eaa28b345202f2bdf8361e06f843bb7a010b7d8f80132f742672c94249c43f64031cefa161e415e2e2ab3a53b23070fb63854283f9e040f5ff79394ac7d1  saned.initd
c7523b2684726cf35c0b251fe2e1863120284ff6ea3f93b53feb5dfa020c1e383910ecdd1a0c77a2289912ac4fd355cb3c743ea3706dab1bcf0a3412e1d0fbcc  endian.patch
1779ff8beb1ba5f9238c25d819a7f0045f7e257c19b511315feb85650e445ca86450a9e1d7ff8650499d3dae808589a6c2e358d5f3f39a3f40ce4999179b86d6  include.patch
9cb595841f59b5105ecc85e4c0ad8781c52caa2354fb823c920ec467e88afbe47f2b3f4a7a3980bef5dbf22983c5786f051a9d10aea97b4bf7c4a05378592029  network.patch
8f0a1529a5793bc78422419b674963b543527c932476c9ea2d92ea0ad0a286691da306020824c1aaa0b35929f571480d21d7fc464a9f652e15664854c75a4cea  pidfile.patch
4de6f60452c0451769f5ce41e41ca4c2867a723e0d2bf22796dc8a266359bdc8a9e9542f4ba2dc42b15bd25b1c83d2c339177796043fdbcbc9d73ad4957f723c  check.patch"
