# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=labplot
pkgver=2.7.0
pkgrel=0
pkgdesc="Interactive tool for graphing and analysis of scientific data"
url="https://www.kde.org/applications/education/labplot2"
arch="all"
options="!check"  # all tests require X11
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	karchive-dev kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kio-dev
	kdoctools-dev ki18n-dev kiconthemes-dev kdelibs4support-dev kxmlgui-dev
	knewstuff-dev ktextwidgets-dev kwidgetsaddons-dev gsl-dev fftw-dev
	qt5-qtserialport-dev syntax-highlighting-dev bison libexecinfo-dev
	cantor-dev docbook-xsl lz4-dev poppler-dev poppler-qt5-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/labplot/$pkgver/labplot-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# gives incorrect results
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E fittest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="73e10ed98694b7633e80ffa7745733392cf9ce36f45e4ef6432d661b015dd3e821ddc791223a32fa55115c68f0d82b90464080915f8bb55cd1907610151a2d0a  labplot-2.7.0.tar.xz"
