# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt-creator
pkgver=4.11.0
pkgrel=0
pkgdesc="Cross-platform multi-language programming IDE"
url="https://doc.qt.io/qtcreator/index.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.0 WITH Qt-LGPL-exception-1.1"
depends="qt5-qtquickcontrols"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtserialport-dev
	qt5-qtscript-dev qt5-qttools-dev clang-dev llvm8-dev python3
	libexecinfo-dev"
subpackages="$pkgname-dev"
source="https://download.qt.io/official_releases/qtcreator/${pkgver%.*}/$pkgver/qt-creator-opensource-src-$pkgver.tar.gz"
ldpath="/usr/lib/qtcreator"
builddir="$srcdir/$pkgname-opensource-src-$pkgver"

build() {
	export LLVM_INSTALL_DIR=/usr
	qmake -r "QMAKE_CFLAGS += $CFLAGS" "QMAKE_CXXFLAGS += $CXXFLAGS" "QMAKE_LFLAGS += $LDFLAGS" "QMAKE_LIBS += -lexecinfo"
	make
}

package() {
	make install INSTALL_ROOT="$pkgdir"/usr
}

sha512sums="29e5c72d4002cdddecbdea689dd5048aa8d0b74c980a6b8096b03c3c39f4868e4490c4516ba287a4082eaaa4ec412309c1d498a4f97754720c88ae4ab53d30b8  qt-creator-opensource-src-4.11.0.tar.gz"
