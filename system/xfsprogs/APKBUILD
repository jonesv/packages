# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xfsprogs
pkgver=5.4.0
pkgrel=0
pkgdesc="XFS filesystem utilities"
url="https://xfs.org/index.php/Main_Page"
arch="all"
options="!check"  # No test suite.
license="GPL-1.0-only"
depends="$pkgname-base"
makedepends="attr-dev bash icu-dev libedit-dev linux-headers util-linux-dev"
subpackages="$pkgname-base $pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://www.kernel.org/pub/linux/utils/fs/xfs/$pkgname/$pkgname-$pkgver.tar.gz
	fix-mmap.patch
	no-utmp-header.patch
	"

build() {
	export DEBUG=-DNDEBUG
	export OPTIMIZER="$CFLAGS"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/sbin \
		--libexecdir=/usr/lib \
		--enable-editline
	make SHELL=/bin/bash
}

check() {
	make check
}

package() {
	make -j1 DIST_ROOT="$pkgdir" install install-dev
	find "$pkgdir" -name *.la -delete
	chown -R 0:0 "$pkgdir"
}

base() {
	# Everything except xfs_scrub, which pulls in 50 MiB of icu libs.
	pkgdesc="Base $pkgdesc"
	mkdir "$subpkgdir"
	mv "$pkgdir"/sbin "$subpkgdir"/
	mkdir "$pkgdir"/sbin
	mv "$subpkgdir"/sbin/xfs_scrub "$pkgdir"/sbin/
}

sha512sums="4d520881f4556976c3d64c5f72f51ea5531c7f380b22c48a1fb72bfde078ac725c70899c8c58263aa2a7aed2213de604121f2863e88b5cf8fb0dcc54446e133e  xfsprogs-5.4.0.tar.gz
c23d5dca744c4589ede517701fc8ea02f9b7a59568d907269048741806d2e6c9e56ed3493163d63dbf16193ff99471206548b25efcda18e3e5dff14eb38066d4  fix-mmap.patch
29c77c550fa8f7a0b3794649d278a2cb3a65c9368db19415866910160adb6d5a52f1be4f8713b58c5c87f149b6212c068ae27a4547a6c4e4fe7b1584e1261dae  no-utmp-header.patch"
