# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libffi
pkgver=3.2.1
pkgrel=6
pkgdesc="A portable foreign function interface library"
url="https://sourceware.org/libffi"
arch="all"
license="MIT"
depends=""
makedepends="texinfo"
checkdepends="dejagnu"
subpackages="$pkgname-dev $pkgname-doc"
source="https://sourceware.org/pub/$pkgname/$pkgname-$pkgver.tar.gz
	disable-ppc-ldvariant.patch
	fix-testsuite-longdouble.patch
	fix-testsuite-opts.patch
	gnu-linux-define.patch
	powerpc-fixes.patch
	"

build () {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	make check
}

package() {
	make DESTDIR=""$pkgdir"" install
	install -m755 -d ""$pkgdir"/usr/share/licenses/$pkgname"
	install -m644 LICENSE ""$pkgdir"/usr/share/licenses/$pkgname/"
	# fix location for headers
	# see also: https://github.com/libffi/libffi/issues/24
	mkdir -p "$pkgdir"/usr/include/
	mv "$pkgdir"/usr/lib/libffi-$pkgver/include/*.h \
		"$pkgdir"/usr/include/
	rmdir "$pkgdir"/usr/lib/libffi-$pkgver/include || true
	sed -i -e '/^includedir=/{s,=.*,=/usr/include,g}' \
		"$pkgdir"/usr/lib/pkgconfig/libffi.pc
}

sha512sums="980ca30a8d76f963fca722432b1fe5af77d7a4e4d2eac5144fbc5374d4c596609a293440573f4294207e1bdd9fda80ad1e1cafb2ffb543df5a275bc3bd546483  libffi-3.2.1.tar.gz
cfd3b11a0e168fd74da0a6219c95610df3466b0769966351b2a5076c93a75996daf9aed41644bebb80e28793bbe18d62272385afd7813c472104cc6c93dcba41  disable-ppc-ldvariant.patch
de92cb20ded7bfefc3e469ba2ac2d9d869d67dc172ec7e2d1222f8530944eb6d5016ae913baf01ac2e26bee1624c682ae9dd08d0e45d5532d59298dbe7e417eb  fix-testsuite-longdouble.patch
9b4b1d1b54cc7c96c44ba727556d9bfd2ab261a594061223df2fbd47ae7a067a7b9a3db92dc2e1b45d8c502bf4a192f8988c7638737c7aacfeac31ed1536891a  fix-testsuite-opts.patch
264af568ae5388d50f647f891a406945c73cc358692266f65ad341787c0bf5f6bf31203b86c39fa1b338101c1a6d2f4fec60f95a90d379951ff5153f8f9e178f  gnu-linux-define.patch
bb3eb51a196aac2849fa0d0214e46f1428d55c8ed4375e5f8cf7fc9076927db935f97ac4d6e61a101897ac6ddc137a0a61e210b8d91888e938c6af122700e935  powerpc-fixes.patch"
