# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=git
pkgver=2.25.4
pkgrel=0
pkgdesc="Distributed version control system"
url="https://www.git-scm.com/"
arch="all"
license="GPL-2.0+"
depends="perl-error"
checkdepends="python3"
makedepends="zlib-dev openssl-dev curl-dev expat-dev perl-dev
	pcre2-dev asciidoctor xmlto perl-error docbook-xsl-ns"
# note that order matters
subpackages="$pkgname-doc
	$pkgname-bash-completion:completion:noarch
	$pkgname-email
	$pkgname-fast-import:_fast_import
	$pkgname-cvs::noarch
	$pkgname-p4::noarch
	$pkgname-daemon
	$pkgname-gitweb
	$pkgname-subtree::noarch
	$pkgname-lang
	"
replaces="git-perl perl-git"
source="https://www.kernel.org/pub/software/scm/git/git-$pkgver.tar.xz
	dont-test-other-encodings.patch
	git-daemon.initd
	git-daemon.confd
	"
_gitcoredir=/usr/libexec/git-core

# secfixes:
#   2.25.4-r0:
#     - CVE-2020-11008
#   2.25.3-r0:
#     - CVE-2020-5260
#   2.24.1-r0:
#     - CVE-2019-1348
#     - CVE-2019-1349
#     - CVE-2019-1350
#     - CVE-2019-1351
#     - CVE-2019-1352
#     - CVE-2019-1353
#     - CVE-2019-1354
#     - CVE-2019-1387
#     - CVE-2019-19604
#   2.19.2-r0:
#     - CVE-2018-19486
#   2.14.1-r0:
#     - CVE-2017-1000117

prepare() {
	default_prepare
	cat >> config.mak <<-EOF
		NO_SVN_TESTS=YesPlease
		NO_REGEX=YesPlease
		USE_ASCIIDOCTOR=1
		USE_LIBPCRE2=YesPlease
		NO_NSEC=YesPlease
		NO_SYS_POLL_H=1
		CFLAGS=$CFLAGS
	EOF
}

build() {
	make prefix=/usr DESTDIR="$pkgdir"
}

check() {
	rm t/t9020-remote-svn.sh  # Requires /usr/bin/python / python-compat.
	make prefix=/usr DESTDIR="$pkgdir" -j1 test
}

package() {
	make -j1 prefix=/usr \
		DESTDIR="$pkgdir" \
		INSTALLDIRS=vendor \
		install
	mkdir -p "$pkgdir"/var/git
	install -Dm755 "$srcdir"/git-daemon.initd \
		"$pkgdir"/etc/init.d/git-daemon
	install -Dm644 "$srcdir"/git-daemon.confd \
		"$pkgdir"/etc/conf.d/git-daemon

	make prefix=/usr DESTDIR="$pkgdir" install-man
	find "$pkgdir" -name perllocal.pod -delete
}

email() {
	depends="perl perl-net-smtp-ssl perl-authen-sasl"
	pkgdesc="Git tools for sending email"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/*email* "$pkgdir"/$_gitcoredir/*imap* \
		"$subpkgdir"/$_gitcoredir
}

cvs() {
	pkgdesc="Git tools for importing CVS repositories"
	depends="perl cvs perl-dbd-sqlite"
	replaces="git-perl"
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/usr/bin/git-cvs* "$subpkgdir"/usr/bin/
	mv "$pkgdir"/$_gitcoredir/*cvs* "$subpkgdir"/$_gitcoredir \

}

_fast_import() {
	pkgdesc="Git backend for fast Git data importers"
	depends="git=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/git-fast-import "$subpkgdir"/$_gitcoredir/
}

p4() {
	pkgdesc="Git tools for working with Perforce depots"
	depends="git=$pkgver-r$pkgrel git-fast-import=$pkgver-r$pkgrel"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir/mergetools
	mv "$pkgdir"/$_gitcoredir/*p4* "$subpkgdir"/$_gitcoredir/
	mv "$pkgdir"/$_gitcoredir/mergetools/*p4* \
		"$subpkgdir"/$_gitcoredir/mergetools/
}

daemon() {
	pkgdesc="Git protocol daemon"
	depends="git=$pkgver-r$pkgrel"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/git-daemon \
		"$pkgdir"/$_gitcoredir/git-http-backend \
		"$pkgdir"/$_gitcoredir/git-shell \
		"$subpkgdir"/$_gitcoredir \

	mv "$pkgdir"/etc "$subpkgdir"/
}

gitweb() {
	pkgdesc="Simple web interface to git repositories"
	depends="git=$pkgver-r$pkgrel perl"
	replaces="git"
	mkdir -p "$subpkgdir"/usr/share "$subpkgdir"$_gitcoredir
	mv "$pkgdir"/usr/share/gitweb "$subpkgdir"/usr/share/
	mv "$pkgdir"/$_gitcoredir/git-instaweb "$subpkgdir"$_gitcoredir
}

completion() {
	pkgdesc="Bash completion for $pkgname"
	depends=""
	replaces=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	install -Dm644 "$builddir"/contrib/completion/git-completion.bash \
		"$subpkgdir"/usr/share/bash-completion/completions/git
}

subtree() {
	depends="git=$pkgver-r$pkgrel"
	pkgdesc="Split git repository into subtrees"
	replaces=""

	cd "$builddir"/contrib/subtree
	make prefix=/usr DESTDIR="$pkgdir"
	make install prefix=/usr DESTDIR="$subpkgdir"
}

sha512sums="ca2ecc561d06dbb393fe47d445f0d69423d114766d9bcc125ef1d6d37e350ad903c456540cea420c1a51635b750cde3901e4196f29ce95b315fda11270173450  git-2.25.4.tar.xz
0a0935d876024d96156df3aeec06b47fd9e370484d4552786c450cb500ae671a631e64c30994ec39f43a2f313f75d68909688ea92b47327d1af65e365dc77480  dont-test-other-encodings.patch
89528cdd14c51fd568aa61cf6c5eae08ea0844e59f9af9292da5fc6c268261f4166017d002d494400945e248df6b844e2f9f9cd2d9345d516983f5a110e4c42a  git-daemon.initd
fbf1f425206a76e2a8f82342537ed939ff7e623d644c086ca2ced5f69b36734695f9f80ebda1728f75a94d6cd2fcb71bf845b64239368caab418e4d368c141ec  git-daemon.confd"
